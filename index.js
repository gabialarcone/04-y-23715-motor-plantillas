//importar express
const express = require("express");
const bodyParser = require("body-parser");
const hbs = require("hbs");
const app = express();
const routerAdmin = require("./routes/admin");
const router = require("./routes/public");


app.use(bodyParser.urlencoded({ extended:true }));
app.use("/", router);
app.use("/admin", routerAdmin);
app.use(express.static(__dirname + "/public"));
app.set('view engine', 'hbs');
app.set('views', __dirname + "/views");

hbs.registerPartials(__dirname + "/views/partials");

app.use((req, res, next) => {
    res.status(404).render('aviso'); // Renderizar el archivo mensaje.hbs desde la carpeta partials
});

// Iniciar el servidor en el puerto 3000

const puerto = process.env.PORT  || 3000;
app.listen(puerto, () => {
    console.log("El servidor se está ejecutando en http://localhost:" + puerto);
});

