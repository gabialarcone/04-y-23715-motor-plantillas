const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const dbPath = path.resolve(__dirname, 'integrantes.sqlite');
const db = new sqlite3.Database(dbPath);


db.serialize(() => {
    db.run('SELECT 1', [], (err) => {
        if (err) {
            console.error('Error al conectar con la base de datos:', err);
        } else {
            console.log('Conexión exitosa con la base de datos.');
            db.run("select * from integrante");
        }
    });
});

async function getAll(query, parametros){
    return new Promise((resolve, reject)=>{
        db.all(query, parametros, (error, rows)=>{
            if (error) {
                reject(error);
            } else{
                resolve(rows);
            }
        });
    });
}

async function getLastOrder(){
    const result = await getAll("select  Max(nro_orden) as maxOrden from integrante");
    return result[0].maxOrden;
}

async function insert(query, parametros) {
    return new Promise((resolve, reject) => {
        db.run(query, parametros, function(error) {
            if (error) {
                reject(error);
            } else {
                resolve(this.lastID);
            }
        });
    });
}

async function getLastOrderTipoMedia() {
    try {
        const result = await query('SELECT MAX(nro_orden) as maxOrder FROM tipomedia');
        return result[0].maxOrder || 0;
    } catch (error) {
        console.error('Error al obtener el último orden de tipo media:', error);
        throw error;
    }
}

async function getLastId(tablaNombre) {
    const result = await getAll(`SELECT MAX(id_tmedia) as maxId FROM ${tablaNombre}`);
    return result[0].maxId;
}


function runQuery(query, params) {
    return new Promise((resolve, reject) => {
        db.run(query, params, function (err) {
            if (err) {
                return reject(err);
            }
            resolve(this.lastID); // Devuelve el ID de la última fila insertada
        });
    });
}
module.exports = {
    db,
    getAll,
    insert,
    getLastOrder,
    getLastOrderTipoMedia,
    getLastId,
    runQuery,

}

