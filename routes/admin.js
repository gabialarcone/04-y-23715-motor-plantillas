const express = require("express");
const { getAll, getLastOrder, insert, runQuery, getLastId} = require("../db/conexion");
const router = express.Router();
const multer = require("multer");
const path = require("path");

// Configuración de Multer
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/Images/');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname));
    }
});

const upload = multer({ storage: storage });

router.get('/', (req, res) => {
    res.render("admin/index");
});

router.get('/integrante/listar', async (req, res) => {
    const rows = await getAll("select * from integrante");
    console.log(rows);
    res.render("admin/integrante/index", {
        integrantes: rows,
    });
});

router.get('/integrante/crear', async (req, res) => {
    res.render('admin/integrante/crearForm');
});

router.post('/integrante/create', async (req, res) => {
    const lastOrder = await getLastOrder("integrante");
    const newOrder = lastOrder + 1;
    const { nombre, apellido, matricula, pagina, activo } = req.body;
    const isActive = activo === 'on' ? 1 : 0;

    // Validar los datos
    const errors = [];
    if (!matricula || typeof matricula !== 'string' || matricula.trim().length < 3 || !/^[a-zA-Z0-9]+$/.test(matricula)) {
        errors.push('Matricula inválida. Debe tener al menos 3 caracteres alfanuméricos.');
    }
    if (!nombre || typeof nombre !== 'string' || nombre.trim().length < 2 || !/^[a-zA-Z]+$/.test(nombre)) {
        errors.push('Nombre inválido. Debe tener al menos 2 letras.');
    }
    if (!apellido || typeof apellido !== 'string' || apellido.trim().length < 2 || !/^[a-zA-Z]+$/.test(apellido)) {
        errors.push('Apellido inválido. Debe tener al menos 2 letras.');
    }


    if (errors.length > 0) {
        return res.status(400).render('admin/integrante/crearForm', {
            errors,
            data: req.body,
        });
    }
    try {
        // Insertar los datos en la tabla integrantes
        await insert('INSERT INTO integrante (nro_orden, matricula, nombre, apellido, pagina, activo) VALUES (?, ?, ?, ?, ?, ?)',
            [newOrder, matricula, nombre, apellido, pagina, isActive]);
        // Redireccionar al usuario al listado de integrantes después de una inserción exitosa
        res.redirect(`/admin/integrante/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
    } catch (error) {
        console.error(error);
        res.redirect(`/admin/integrante/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
    }
});

router.get("/media/listar", async (req, res) => {
    try {
        const media = await getAll(`
            SELECT media.*,
                   tipomedia.nombre AS tipomediaNombre,
                   integrante.nombre AS integranteNombre,
                   integrante.apellido AS integranteApellido
            FROM media
                     LEFT JOIN tipomedia ON media.id_tmedia = tipomedia.id_tmedia
                     LEFT JOIN integrante ON media.matricula = integrante.matricula
            WHERE media.activo = 1
            ORDER BY media.nro_orden
        `, []);

        res.render('admin/media/index', { media });
    } catch (error) {
        console.error(error);
        res.status(500).send('Hubo un error al listar los medios. Por favor, inténtelo de nuevo.');
    }
});

router.get('/media/crear', async (req, res) => {
    try {
        const integrantes = await getAll('SELECT * FROM integrante');
        res.render('admin/media/crearMedia', { integrantes });
    } catch (error) {
        console.error(error);
        res.status(500).send('Hubo un error al cargar los datos necesarios. Por favor, inténtelo de nuevo.');
    }
});



router.get('/tipomedia/listar', async (req, res) => {
    const rows = await getAll("SELECT * FROM tipomedia");
    console.log(rows);
    res.render("admin/tipomedia/index", {
        tipomedia: rows,
    });
});

router.get('/tipomedia/crear', async (req, res) => {
    res.render('admin/tipomedia/crearTipoMedia');
});

router.post("/tipomedia/create", async (req, res) => {
    const ultimoOrden = await getLastOrder("tipomedia");
    const nuevoOrden = ultimoOrden + 1;
    const ultimoId = await getLastId("tipomedia");
    const nuevoId = ultimoId + 1;

    const { nombre, activo } = req.body;
    const isActive = activo ? 1 : 0;

    if (!nombre) {
        return res.status(400).json({ error: 'Debes completar todos los campos.' });
    }

    try {
        await insert('INSERT INTO tipomedia (nro_orden, id_tmedia, nombre, activo) VALUES (?, ?, ?, ?)',
            [nuevoOrden, nuevoId, nombre, isActive]);
        res.redirect(`/admin/tipomedia/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
    } catch (error) {
        console.error('Error al crear tipo media:', error);
        res.status(500).json({ error: 'Error al crear tipo media' });
    }
});



router.post("/media/create", upload.single('imagen'), async (req, res) => {
    try {
        const { url, titulo, id_tmedia, matricula, activo } = req.body;
        const isActive = activo === 'on' ? 1 : 0;
        let newPath = '';
        if (req.file) {
            newPath = `/Images/${req.file.filename}`;
        }
        if (!newPath && !url) {
            return res.status(400).json({ error: 'Debe proporcionar una URL o cargar una imagen.' });
        }
        const ultimoOrden = await getLastOrder("media");
        const nuevoOrden = ultimoOrden + 1;

        const query = `
            INSERT INTO media (src, url, titulo, id_tmedia, matricula, activo, nro_orden)
            VALUES (?, ?, ?, ?, ?, ?, ?)
        `;
        const params = [newPath, url, titulo, id_tmedia, matricula, isActive, nuevoOrden];
        await runQuery(query, params);

        res.redirect(`/admin/media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
    } catch (error) {
        console.error('Error al crear media:', error);
        res.status(500).json({ error: 'Error al crear media' });
    }
});



module.exports = router;



