// archivo que contiene todas las rutas públicas
const express = require("express");
const router = express.Router();
const {getAll} = require("../db/conexion");

require("dotenv").config();
router.use((req, res, next) => {
    res.locals.repositorio = process.env.REPOSITORIO;
    res.locals.nombre = process.env.NOMBRE;
    res.locals.materia = process.env.MATERIA;
    res.locals.matricula = process.env.MATRICULA;
    next();
});

// definir todas rutas del proyecto
router.get("/", async (request, response) => {
    const rows = await getAll("select * from integrante");
    console.log(rows);
    response.render("index", {
        integrantes: rows,
        isHome: true,

    });
});

router.get("/curso/", async (request, response) => {
    const rows = await getAll("select * from integrante");
    console.log(rows);
    response.render("curso", {
        integrantes: rows,

    });
});


router.get("/word_cloud/", async (request, response) => {
    const rows = await getAll("select * from integrante");
    console.log(rows);
    response.render("word_cloud", {
        integrantes: rows,

    });
});

router.get("/integrante/:matricula", async (request, response, next) => {
    const matriculas = (await getAll("select matricula from integrante where activo = 1 order by nro_orden")).map(obj => obj.matricula);
    const tipoMedia = await getAll("select * from tipomedia where activo = 1 order by nro_orden");
    const matricula = request.params.matricula;
    //console.log(matricula)*/
    const integrantes = await getAll("select * from integrante");
    //console.log(matriculas)*/
    if (matriculas.includes(matricula)) {
        const integranteFilter = await getAll("select * from integrante where matricula = ?", [matricula]);
        const mediaFilter = await getAll("select * from media where matricula = ?", [matricula]);
       // console.log("integranteFilter",integranteFilter)
        //console.log("mediaFilter", mediaFilter)
        response.render('integrantes', {
            integrante: integranteFilter,
            tipoMedia: tipoMedia,
            media: mediaFilter,
            integrantes: integrantes,

        });
    } else {
        console.log("error")
    }
});




// exportamos le router
    module.exports = router;
